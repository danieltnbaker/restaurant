package dev.dandroid.account.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.account.ui.AccountFragment

@Module
abstract class AccountBuilderModule {

    @ContributesAndroidInjector(modules = [AccountModule::class])
    abstract fun bindAccountFragment(): AccountFragment
}