package dev.dandroid.account.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.core.di.ViewModelKey
import dev.dandroid.account.ui.AccountViewModel

@Module
abstract class AccountModule {

    @Binds
    @IntoMap
    @ViewModelKey(AccountViewModel::class)
    abstract fun bindAccountViewModel(viewModel: AccountViewModel): ViewModel
}