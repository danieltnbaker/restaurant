package dev.dandroid.account.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import dev.dandroid.account.R
import dev.dandroid.core.ui.BaseFragment
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : BaseFragment() {

    private lateinit var accountViewModel: AccountViewModel

    override fun layoutRes(): Int = R.layout.fragment_account

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accountViewModel = getViewModel(AccountViewModel::class.java)
        accountViewModel.text.observe(viewLifecycleOwner, Observer {
            textAccount.text = it
        })
    }
}
