package dev.dandroid.mvvm

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import dev.dandroid.mvvm.di.component.DaggerAppComponent

class MvvmApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<MvvmApplication> {
        return DaggerAppComponent.builder()
                .application(this)
                .build()
    }
}