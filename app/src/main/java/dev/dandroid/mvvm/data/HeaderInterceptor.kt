package dev.dandroid.mvvm.data

import okhttp3.Interceptor
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HeaderInterceptor @Inject constructor() : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val sonicRequestRequest = chain.request().newBuilder()
            .header(HEADER_SECRET_KEY, HEADER_SECRET_KEY_VALUE)
            .build()
        return chain.proceed(sonicRequestRequest)
    }

    companion object {

        private const val HEADER_SECRET_KEY = "secret-key"
        private const val HEADER_SECRET_KEY_VALUE =
            "\$2b\$10\$Ukzn4V8enPkE4sUwycF0VOC10jtosXLcdf5qRHveQ2.L6D9f12edC"
    }
}