package dev.dandroid.mvvm.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dev.dandroid.mvvm.MvvmApplication
import dev.dandroid.mvvm.di.module.AppModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class
])
interface AppComponent : AndroidInjector<MvvmApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: MvvmApplication): Builder

        fun build(): AppComponent
    }
}