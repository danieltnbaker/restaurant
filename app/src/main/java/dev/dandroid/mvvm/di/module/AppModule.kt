package dev.dandroid.mvvm.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.android.AndroidInjectionModule
import dev.dandroid.core.di.ApplicationContext
import dev.dandroid.mvvm.MvvmApplication
import javax.inject.Singleton

@Module(includes = [
    AndroidInjectionModule::class,
    NetworkModule::class,
    ViewModelModule::class,
    BindingsModule::class
])
class AppModule {

    @Provides
    @Singleton
    @ApplicationContext
    fun provideContext(application: MvvmApplication): Context = application.applicationContext
}