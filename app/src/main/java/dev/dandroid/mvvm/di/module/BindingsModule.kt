package dev.dandroid.mvvm.di.module

import dagger.Module
import dev.dandroid.account.di.AccountBuilderModule
import dev.dandroid.details.di.DetailsBuilderModule
import dev.dandroid.list.di.RestaurantListBuilderModule
import dev.dandroid.map.di.MapBuilderModule

@Module(includes = [
    MainBindingModule::class,
    RestaurantListBuilderModule::class,
    MapBuilderModule::class,
    AccountBuilderModule::class,
    DetailsBuilderModule::class
])
abstract class BindingsModule