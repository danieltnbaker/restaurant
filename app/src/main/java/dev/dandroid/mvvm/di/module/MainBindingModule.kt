package dev.dandroid.mvvm.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.mvvm.ui.NavigationActivity

@Module
abstract class MainBindingModule {

    @ContributesAndroidInjector(modules = [NavigationModule::class])
    abstract fun bindNavigationActivity(): NavigationActivity
}