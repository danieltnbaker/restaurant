package dev.dandroid.mvvm.di.module

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.core.di.ViewModelKey
import dev.dandroid.mvvm.util.NavigationViewModel

@Module
abstract class NavigationModule {

    @Binds
    @IntoMap
    @ViewModelKey(NavigationViewModel::class)
    abstract fun bindNavigationViewModel(viewModel: NavigationViewModel): ViewModel
}