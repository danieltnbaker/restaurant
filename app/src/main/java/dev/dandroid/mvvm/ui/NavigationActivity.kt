package dev.dandroid.mvvm.ui

import android.os.Bundle
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import dev.dandroid.core.ui.BaseActivity
import dev.dandroid.mvvm.R
import dev.dandroid.mvvm.util.NavigationViewModel
import kotlinx.android.synthetic.main.activity_nav.*

class NavigationActivity : BaseActivity() {

    override fun layoutRes(): Int = R.layout.activity_nav

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel = getViewModel(NavigationViewModel::class.java)

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.navHostFragment) as NavHostFragment

        bottomNavigationView.setupWithNavController(navHostFragment.navController)
    }
}
