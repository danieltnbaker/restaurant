package dev.dandroid.mvvm.util

import dev.dandroid.core.presentation.BaseViewModel
import javax.inject.Inject

class NavigationViewModel @Inject constructor(): BaseViewModel()