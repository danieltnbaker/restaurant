package dev.dandroid.commonmodels.domain

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Restaurant(
    val name: String,
    val address: String,
    val cuisine: String,
    val description: String,
    val menu: String,
    val thumb: String,
    val image: String,
    val latitude: Double,
    val longitude: Double
): Parcelable