package dev.dandroid.commonmodels.network

import com.google.gson.annotations.SerializedName

data class RestaurantDto(
    @SerializedName("name")
    val name: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("cuisine")
    val cuisine: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("menu")
    val menu: String,
    @SerializedName("thumbnail")
    val thumbnail: String,
    @SerializedName("image")
    val image: String,
    @SerializedName("latitude")
    val latitude: Double,
    @SerializedName("longitude")
    val longitude: Double
)