package dev.dandroid.core.data.domain

import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.data.mapper.RestaurantMapper
import dev.dandroid.core.data.rest.RestaurantRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetRestaurantsUseCase @Inject constructor(
    private val restaurantRepository: RestaurantRepository,
    private val restaurantMapper: RestaurantMapper
) {

    fun getRestaurants(): Single<List<Restaurant>> {
        return restaurantRepository.getRestaurants()
            .observeOn(Schedulers.computation())
            .map { restaurantMapper.mapToDomain(it) }
    }
}