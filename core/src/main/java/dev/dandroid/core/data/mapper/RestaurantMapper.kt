package dev.dandroid.core.data.mapper

import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.commonmodels.network.RestaurantDto
import javax.inject.Inject

class RestaurantMapper @Inject constructor() {

    fun mapToDomain(restaurantDtoList: List<RestaurantDto>): List<Restaurant> {
        return restaurantDtoList.map {
            Restaurant(
                it.name,
                it.address,
                it.cuisine,
                it.description,
                it.menu,
                it.thumbnail,
                it.image,
                it.latitude,
                it.longitude
            )
        }
    }
}