package dev.dandroid.core.data.rest

import dev.dandroid.commonmodels.network.RestaurantDto
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RestaurantRepository @Inject constructor(private val restaurantService: RestaurantService) {

    fun getRestaurants(): Single<List<RestaurantDto>> {
        return restaurantService.getRestaurants()
                .subscribeOn(Schedulers.io())
    }
}