package dev.dandroid.core.data.rest

import dev.dandroid.commonmodels.network.RestaurantDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface RestaurantService {

    @GET("restaurants.json")
    fun getRestaurants(): Single<List<RestaurantDto>>
}