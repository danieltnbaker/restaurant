package dev.dandroid.core.di

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext
