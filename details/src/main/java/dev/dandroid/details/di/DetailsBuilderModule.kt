package dev.dandroid.details.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.details.ui.RestaurantDetailsFragment

@Module
abstract class DetailsBuilderModule {

    @ContributesAndroidInjector(modules = [DetailsModule::class])
    abstract fun bindDetailsFragment(): RestaurantDetailsFragment
}