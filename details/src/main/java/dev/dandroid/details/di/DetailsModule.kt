package dev.dandroid.details.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.core.di.ViewModelKey
import dev.dandroid.details.ui.RestaurantDetailsViewModel

@Module
abstract class DetailsModule {

    @Binds
    @IntoMap
    @ViewModelKey(RestaurantDetailsViewModel::class)
    abstract fun bindDetailsViewModel(viewModel: RestaurantDetailsViewModel): ViewModel
}
