package dev.dandroid.details.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.ui.BaseFragment
import dev.dandroid.details.R
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.fragment_details.view.*
import kotlinx.android.synthetic.main.fragment_details.view.toolbar


class RestaurantDetailsFragment : BaseFragment(), OnMapReadyCallback {

    private val fragmentArgs: RestaurantDetailsFragmentArgs by navArgs()
    private var googleMap: GoogleMap? = null

    private lateinit var viewModel: RestaurantDetailsViewModel

    override fun layoutRes(): Int = R.layout.fragment_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        toolbar.apply {
            navigationIcon = ResourcesCompat.getDrawable(
                resources,
                R.drawable.ic_arrow_back_black_24dp,
                view.context.theme
            )
            setNavigationOnClickListener { activity?.onBackPressed() }
        }

        viewModel = getViewModel(RestaurantDetailsViewModel::class.java)
        viewModel.restaurantData.observe(viewLifecycleOwner, Observer { setView(it) })
        viewModel.mapData.observe(viewLifecycleOwner, Observer { setMapMarker(it) })
        viewModel.onRestaurantReceived(fragmentArgs.restaurant)

        addMapFragment()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap

        googleMap?.let {
            it.uiSettings.isRotateGesturesEnabled = false
            it.uiSettings.isZoomGesturesEnabled = false
            it.uiSettings.isZoomControlsEnabled = false
            it.uiSettings.isTiltGesturesEnabled = false
        }

        viewModel.onMapReady()
    }

    private fun addMapFragment() {
        val fm: FragmentManager = childFragmentManager
        val supportMapFragment = SupportMapFragment.newInstance()
        fm.beginTransaction().replace(R.id.mapContainer, supportMapFragment).commit()
        supportMapFragment.getMapAsync(this)
    }

    private fun setView(restaurant: Restaurant) {
        view?.let {
            it.toolbar.title = restaurant.name
            it.cuisine.text = restaurant.cuisine
            it.address.text = restaurant.address
            it.description.text = restaurant.description

            Glide.with(it)
                .load(restaurant.image)
                .centerInside()
                .placeholder(R.drawable.ic_placeholder)
                .into(object : CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {}

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        it.collapsingToolbar.background = resource
                    }
                })
        }
    }

    private fun setMapMarker(restaurant: Restaurant) {
        googleMap?.let {
            val latLng = LatLng(restaurant.latitude, restaurant.longitude)
            it.addMarker(MarkerOptions().position(latLng))
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, MAP_ZOOM_LEVEL))
        }
    }

    companion object {
        private const val MAP_ZOOM_LEVEL: Float = 15f
    }
}