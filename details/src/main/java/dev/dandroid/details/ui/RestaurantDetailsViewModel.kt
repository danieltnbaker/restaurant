package dev.dandroid.details.ui

import androidx.lifecycle.MutableLiveData
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.presentation.BaseViewModel
import javax.inject.Inject

class RestaurantDetailsViewModel @Inject constructor() : BaseViewModel() {

    val restaurantData = MutableLiveData<Restaurant>()
    val mapData = MutableLiveData<Restaurant>()

    private var restaurant: Restaurant? = null

    fun onRestaurantReceived(restaurant: Restaurant) {
        this.restaurant = restaurant
        restaurantData.value = restaurant
    }

    fun onMapReady() {
        mapData.value = restaurant
    }
}