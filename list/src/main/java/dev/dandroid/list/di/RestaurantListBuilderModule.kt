package dev.dandroid.list.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.list.ui.RestaurantListFragment

@Module
abstract class RestaurantListBuilderModule {

    @ContributesAndroidInjector(modules = [RestaurantListModule::class])
    abstract fun bindListFragment(): RestaurantListFragment
}