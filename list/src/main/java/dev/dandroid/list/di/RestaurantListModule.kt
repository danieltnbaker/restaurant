package dev.dandroid.list.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.core.di.ViewModelKey
import dev.dandroid.list.ui.RestaurantListViewModel

@Module
abstract class RestaurantListModule {

    @Binds
    @IntoMap
    @ViewModelKey(RestaurantListViewModel::class)
    abstract fun bindListViewModel(viewModel: RestaurantListViewModel): ViewModel
}