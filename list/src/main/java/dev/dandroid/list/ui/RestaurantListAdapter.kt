package dev.dandroid.list.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.list.R
import dev.dandroid.list.ui.RestaurantListAdapter.AlbumViewHolder
import kotlinx.android.synthetic.main.restaurant_list_item.view.*

class RestaurantListAdapter(val data: List<Restaurant>) : RecyclerView.Adapter<AlbumViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.restaurant_list_item, parent, false)
        return AlbumViewHolder(view)
    }

    override fun onBindViewHolder(holder: AlbumViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int = data.size

    inner class AlbumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(restaurant: Restaurant) {
            itemView.name.text = restaurant.name
            itemView.cuisine.text = restaurant.cuisine.capitalize()
            itemView.setOnClickListener { navigateToRestaurantDetails(restaurant) }

            Glide.with(itemView)
                .load(restaurant.thumb)
                .centerInside()
                .placeholder(R.drawable.ic_placeholder)
                .into(itemView.image)
        }

        private fun navigateToRestaurantDetails(restaurant: Restaurant) {
            val action = RestaurantListFragmentDirections
                .actionNavigationListToNavigationDetails(restaurant)
            itemView.findNavController().navigate(action)
        }
    }
}