package dev.dandroid.list.ui

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.ui.BaseFragment
import dev.dandroid.list.R
import kotlinx.android.synthetic.main.restaurant_list_fragment.*
import kotlinx.android.synthetic.main.restaurant_list_fragment.view.*

class RestaurantListFragment : BaseFragment() {

    override fun layoutRes(): Int = R.layout.restaurant_list_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.apply {
            setActionBar(view.toolbar)
        }

        val viewModel = getViewModel(RestaurantListViewModel::class.java)

        viewModel.loadAlbums.observe(viewLifecycleOwner, Observer {
            it?.let { albums ->
                recyclerView.layoutManager = LinearLayoutManager(context)
                recyclerView.adapter = RestaurantListAdapter(albums)
                recyclerView.visibility = View.VISIBLE
            }
        })
        viewModel.loadError.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                recyclerView.visibility = View.GONE
                errorTextView.visibility = View.VISIBLE
                errorTextView.text = getString(R.string.list_error_message)
            } else {
                errorTextView.visibility = View.GONE
                errorTextView.text = null
            }
        })
        viewModel.loading.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                swipeRefreshLayout.isRefreshing = true
                recyclerView.visibility = View.GONE
                errorTextView.visibility = View.GONE
            } else {
                swipeRefreshLayout.isRefreshing = false
            }
        })

        swipeRefreshLayout.setOnRefreshListener { viewModel.loadRestaurants() }

        viewModel.loadRestaurants()
    }
}