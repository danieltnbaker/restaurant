package dev.dandroid.list.ui

import androidx.lifecycle.MutableLiveData
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.presentation.BaseViewModel
import dev.dandroid.core.data.domain.GetRestaurantsUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class RestaurantListViewModel @Inject constructor(private val getRestaurantsUseCase: GetRestaurantsUseCase) : BaseViewModel() {

    val loadAlbums: MutableLiveData<List<Restaurant>> = MutableLiveData()
    val loadError: MutableLiveData<Boolean> = MutableLiveData()
    val loading: MutableLiveData<Boolean> = MutableLiveData()

    fun loadRestaurants() {
        loading.value = true

        disposables.add(getRestaurantsUseCase.getRestaurants()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<Restaurant>>() {
                    override fun onSuccess(value: List<Restaurant>) {
                        loadError.value = false
                        loadAlbums.value = value
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        loading.value = false
                    }
                }))
    }
}