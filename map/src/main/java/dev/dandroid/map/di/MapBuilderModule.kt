package dev.dandroid.map.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dev.dandroid.map.ui.MapFragment

@Module
abstract class MapBuilderModule {

    @ContributesAndroidInjector(modules = [MapModule::class])
    abstract fun bindMapFragment(): MapFragment
}