package dev.dandroid.map.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dev.dandroid.core.di.ViewModelKey
import dev.dandroid.map.ui.MapViewModel

@Module
abstract class MapModule {

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindMapViewModel(viewModel: MapViewModel): ViewModel
}