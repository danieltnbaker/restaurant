package dev.dandroid.map.ui

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.ui.BaseFragment
import dev.dandroid.map.R


class MapFragment : BaseFragment(), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    private val mapViewModel by navGraphViewModels<MapViewModel>(R.id.map_graph)

    override fun layoutRes(): Int = R.layout.fragment_map

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        mapViewModel = getViewModel(MapViewModel::class.java)
        mapViewModel.loadRestaurants.observe(viewLifecycleOwner, Observer {
            loadMapMarkers(it)
        })

        addMapFragment()
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap

        googleMap?.let { map ->
            map.uiSettings?.isZoomControlsEnabled = false
            map.uiSettings?.isMapToolbarEnabled = false
            map.setOnMarkerClickListener { marker ->
                mapViewModel.onMarkerClicked(marker.position) { showDetailsView(it) }
                false
            }
        }

        mapViewModel.getRestaurants()
    }

    private fun addMapFragment() {
        val fm: FragmentManager = childFragmentManager
        val supportMapFragment = SupportMapFragment.newInstance()
        fm.beginTransaction().replace(R.id.mapContainerMap, supportMapFragment, "map").commit()
        supportMapFragment.getMapAsync(this)
    }

    private fun loadMapMarkers(markers: List<Restaurant>) {
        googleMap?.let {
            if (!isResumed) return
            val builder = LatLngBounds.Builder()
            markers.forEach { restaurant ->
                val latLng = LatLng(restaurant.latitude, restaurant.longitude)
                it.addMarker(buildMarker(restaurant, latLng))
                builder.include(latLng)
            }
            val bounds = builder.build()
            val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_BOUNDS_PADDING)
            it.animateCamera(cameraUpdate)
        }
    }

    private fun showDetailsView(restaurant: Restaurant) {
        val action = MapFragmentDirections.actionNavigationMapToNavigationDetails(restaurant)
        findNavController().navigate(action)
    }

    private fun buildMarker(restaurant: Restaurant, latLng: LatLng): MarkerOptions {
        val markerOptions = MarkerOptions()
            .position(latLng)
            .title(restaurant.name)

        context?.let {
            markerOptions.icon(bitmapDescriptorFromVector(it, R.drawable.ic_food))
        }
        return markerOptions
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

    companion object {
        private const val MAP_BOUNDS_PADDING: Int = 50
    }
}
