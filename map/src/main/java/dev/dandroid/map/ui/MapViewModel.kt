package dev.dandroid.map.ui

import androidx.lifecycle.MutableLiveData
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import dev.dandroid.commonmodels.domain.Restaurant
import dev.dandroid.core.data.domain.GetRestaurantsUseCase
import dev.dandroid.core.presentation.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

class MapViewModel @Inject constructor(private val getRestaurantsUseCase: GetRestaurantsUseCase) : BaseViewModel() {

    val loadRestaurants: MutableLiveData<List<Restaurant>> = MutableLiveData()

    private var restaurants: List<Restaurant>? = null

    fun getRestaurants() {
        disposables.add(getRestaurantsUseCase.getRestaurants()
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSuccess { restaurants = it }
            .subscribeWith(object : DisposableSingleObserver<List<Restaurant>>() {
                override fun onSuccess(value: List<Restaurant>) {
                    loadRestaurants.value = value
                }

                override fun onError(e: Throwable) {
                }
            }))
    }

    fun onMarkerClicked(latLng: LatLng, callback: (Restaurant) -> Any) {
        val restaurant = restaurants?.find {
            latLng.latitude == it.latitude && latLng.longitude == it.longitude
        }
        restaurant?.let { callback.invoke(it) }
    }
}